const Promise = require('bluebird')
const path = require('path')
const gql = require('fake-tag')

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions

  return new Promise((resolve, reject) => {
    const productPageTemplate = path.resolve(
      'src/templates/product-page.component.jsx'
    )
    resolve(
      graphql(
        gql`
            {
                allStrapiProduct {
                    edges {
                        node {
                            id
                            slug
                        }
                    }
                }
            }
        `
      ).then(result => {
        if (result.errors) {
          console.log(result.errors)
          reject(result.errors)
        }
        result.data.allStrapiProduct.edges.forEach(edge => {
          createPage({
            path: `/product/${edge.node.slug}/`,
            component: productPageTemplate,
            context: {
              id: edge.node.id
            }
          })
        })
      })
    )
  })
}

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    node: { fs: 'empty' },
    resolve: {
      alias: {
        '../../theme.config': path.join(__dirname, 'src/semantic-ui/theme.config')
      }
    }
  })
}

exports.onCreatePage = async ({ page, actions }) => {
  const { createPage } = actions

  if (page.path.match(/^\/connect/)) {
    page.matchPath = '/connect/*'
    createPage(page)
  }
}
