import Layout from '../components/layout/layout.component'
import React from 'react'

const User = () => {
  return (
    <Layout>
      <p>User page</p>
    </Layout>
  )
}

export default User
