import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import get from 'lodash/get'
import { Header } from 'semantic-ui-react'
import ProductList from '../components/product/product-list/product-list.component'
import Layout from '../components/layout/layout.component'
import SEO from '../components/SEO/seo.component'

const HomePage = ({ location }) => {
  const data = useStaticQuery(graphql`
      query IndexQuery {
          site {
              siteMetadata {
                  title
              }
          }
          allStrapiProduct {
              edges {
                  node {
                      id
                      name
                      description
                      image {
                          id
                          childImageSharp {
                              fluid(maxWidth: 600) {
                                  ...GatsbyImageSharpFluid
                              }
                          }
                      }
                      slug
                      sku
                      price
                      inStock
                  }
              }
          }
      }
  `)

  const siteTitle = get(data, 'site.siteMetadata.title')
  const products = get(data, 'allStrapiProduct.edges')

  return (
    <Layout location={location}>
      <SEO title={siteTitle}/>
      <Header
        as="h3"
        icon
        textAlign="center"
        style={{
          marginBottom: '2em'
        }}
      >
        <Header.Content
          style={{
            width: '60%',
            margin: '0 auto'
          }}
        >
        </Header.Content>
      </Header>
      <ProductList products={products}/>
    </Layout>
  )
}

export default HomePage
