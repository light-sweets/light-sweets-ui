import React from 'react'
import LoginProvider from '../components/auth/login-provider.component'
import { Router } from '@reach/router'

const Connect = () => {
  return (
    <Router basepath="/connect">
      <LoginProvider path="/:providerName"/>
    </Router>
  )
}

export default Connect
