import React from 'react'
import CartItemList from '../components/cart/cart-list.component'
import Layout from '../components/layout/layout.component'
import SEO from '../components/SEO/seo.component'

const Cart = ({ location }) => {
  return (
    <Layout location={location}>
      <SEO title="Cart"/>
      <CartItemList/>
    </Layout>
  )
}

export default Cart
