import i18n from 'i18next'
import LanguageDetector from 'i18next-browser-languagedetector'
import { initReactI18next } from 'react-i18next'
import en from './locales/en/translation.json'
import ru from './locales/ru/translation.json'
import ua from './locales/ua/translation.json'

i18n.use(LanguageDetector).use(initReactI18next).init({
  resources: {
    en: { translation: en },
    ru: { translation: ru },
    ua: { translation: ua }
  },
  fallbackLng: 'en',
  debug: true,
  interpolation: {
    escapeValue: false
  }
})

export default i18n
