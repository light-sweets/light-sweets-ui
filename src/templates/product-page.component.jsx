import React from 'react'
import { graphql } from 'gatsby'
import get from 'lodash/get'
import ProductSummary
  from '../components/product/product-details/product-summary.component'
import ProductDescription
  from '../components/product/product-details/product-description.component'
import Layout from '../components/layout/layout.component'
import SEO from '../components/SEO/seo.component'

class ProductPageTemplate extends React.PureComponent {
  render () {
    const data = get(this, 'props.data.strapiProduct')
    const slug = data.slug
    const product = {
      ...data,
      mainImage: data.mainImage,
      header: data.name,
      sku: data.sku,
      price: data.price
    }

    return (
      <Layout location={this.props.location}>
        <SEO title={slug}/>
        <ProductSummary product={product}/>
        <ProductDescription {...product} />
      </Layout>
    )
  }
}

export default ProductPageTemplate

export const pageQuery = graphql`
    query ProductQuery($id: String!) {
        strapiProduct(id: {eq: $id}) {
            id
            name
            description
            image {
                id
                childImageSharp {
                    fluid(maxWidth: 400) {
                        ...GatsbyImageSharpFluid
                    }
                }
            }
            slug
            sku
            price
            inStock
        }
    }
`
