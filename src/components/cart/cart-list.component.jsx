import React from 'react'
import { Link } from 'gatsby'
import { Button, Grid, Message } from 'semantic-ui-react'
import { useDispatch, useSelector } from 'react-redux'
import { clearItemFromCart } from '../../redux/cart/cart.slice'
import QuantitySelector from './quantity-selector/quantity-selector.component'
import Price from '../price-container/price-container.component'
import ImageWrapper from '../image-wrapper/image-wrapper.component'
import CartSummaryMobile from './cart-summary-mobile/cart-summary-mobile.component'
import { selectCartItems, selectCartTotal } from '../../redux/cart/cart.selectors'
import { withTranslation } from 'react-i18next'
import CartSummary from './cart-summary.component'

const CartItemList = () => {
  const cartItems = useSelector(selectCartItems)
  const cartTotal = useSelector(selectCartTotal)
  const dispatch = useDispatch()

  if (!cartItems.length) {
    return (
      <Message warning>
        <Message.Header>Your cart is empty</Message.Header>
        <p>
          You will need to add some items to the cart before you can checkout.
        </p>
      </Message>
    )
  }

  return (
    <>
      <Grid divided='vertically'>
        {cartItems.map(item => {
          const { image, name, quantity, price, slug, id } = item
          return (
            <Grid.Row key={id} centered verticalAlign='middle'>
              <Grid.Column mobile={8} tablet={4} computer={4}>
                <ImageWrapper image={image} name={name}/>
              </Grid.Column>
              <Grid.Column mobile={8} tablet={4} computer={4}>
                <Link to={`/product/${slug}/`}>{name}</Link>
              </Grid.Column>
              <Grid.Column mobile={6} tablet={3} computer={3} textAlign='center'>
                <QuantitySelector cartItem={item} quantity={quantity}/>
              </Grid.Column>
              <Grid.Column mobile={7} tablet={4} computer={4} textAlign='center'>
                <Price value={quantity * price}/>
              </Grid.Column>
              <Grid.Column mobile={2} tablet={1} computer={1}>
                <Button secondary
                        basic
                        icon='remove'
                        onClick={() => dispatch(clearItemFromCart(item))}
                />
              </Grid.Column>
            </Grid.Row>
          )
        })}
        <CartSummary cartTotal={cartTotal}/>
      </Grid>
      <CartSummaryMobile cartTotal={cartTotal}/>
    </>
  )
}

export default withTranslation()(CartItemList)
