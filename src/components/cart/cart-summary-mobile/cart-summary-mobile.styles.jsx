import styled from 'styled-components'
import { Media } from '../../../media/media'

export const StickyMedia = styled(Media)`
  {
      position: -webkit-sticky; /* Safari */
      position: sticky;
      bottom: 0;
  }
`
