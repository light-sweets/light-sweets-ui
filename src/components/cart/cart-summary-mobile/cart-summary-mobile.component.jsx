import { Segment } from 'semantic-ui-react'
import Price from '../../price-container/price-container.component'
import React from 'react'
import { StickyMedia } from './cart-summary-mobile.styles'
import { MediaContextProvider } from '../../../media/media'
import CheckoutModal from '../../checkout/modal/checkout-modal.component'

const CartSummaryMobile = ({ cartTotal }) => {
  return (
    <MediaContextProvider>
      <StickyMedia at='mobile'>
        <Segment.Group horizontal>
          <Segment>
            <CheckoutModal/>
          </Segment>
          <Segment>
            <Price value={cartTotal}/>
          </Segment>
        </Segment.Group>
      </StickyMedia>
    </MediaContextProvider>
  )
}

export default CartSummaryMobile
