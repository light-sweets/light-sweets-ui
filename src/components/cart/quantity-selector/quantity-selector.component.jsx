import { Button } from 'semantic-ui-react'
import React, { useState } from 'react'
import { updateItemQuantity } from '../../../redux/cart/cart.slice'
import { useDispatch } from 'react-redux'
import { QuantityDropdown } from './quantity-selector.styles'

const QuantitySelector = ({ cartItem, quantity }) => {
  const dispatch = useDispatch()

  const [options, setOptions] = useState(
    Array.from(Array(quantity < 10 ? 10 : quantity), (_, i) => {
      i++
      return { key: i, text: i, value: i }
    })
  )

  const updateOptions = (quantity) => {
    if (!options.some(option => option.key === quantity)) {
      setOptions(
        options.concat({ key: quantity, text: quantity, value: quantity })
      )
    }
  }

  const updateQuantity = (cartItem, quantity) => {
    updateOptions(quantity)
    dispatch(updateItemQuantity({ item: cartItem, quantity }))
  }

  const handleAddItem = (cartItem, quantity) => {
    const incrementedQuantity = parseInt(quantity) + 1
    updateQuantity(cartItem, incrementedQuantity)
  }

  const handleRemoveItem = (cartItem, quantity) => {
    const decrementedQuantity = parseInt(quantity) - 1
    if (decrementedQuantity > 0) {
      updateQuantity(cartItem, decrementedQuantity)
    }
  }

  const handleChange = (e, cartItem) => {
    const parsedQuantity = parseInt(e.target.innerText)
    updateOptions(parsedQuantity)
    updateQuantity(cartItem, parsedQuantity)
  }

  return (
    <Button.Group selection buttons={
      [
        <Button secondary
                key='minus' icon='minus'
                onClick={() => handleRemoveItem(cartItem, quantity)}
        />,
        <QuantityDropdown secondary
                          key='quantity'
                          button scrolling icon={null}
                          onChange={e => handleChange(e, cartItem)}
                          value={quantity}
                          options={options}
        />,
        <Button secondary
                key='plus' icon='plus'
                onClick={() => handleAddItem(cartItem, quantity)}
        />
      ]
    }/>
  )
}

export default QuantitySelector
