import styled from 'styled-components'
import { Dropdown } from 'semantic-ui-react'

export const QuantityDropdown = styled(Dropdown)`
  .visible.menu.transition {
    max-height: 5rem;
  }
`
