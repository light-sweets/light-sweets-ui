import { Grid, Segment } from 'semantic-ui-react'
import Price from '../price-container/price-container.component'
import React from 'react'
import CheckoutModal from '../checkout/modal/checkout-modal.component'

const CartSummary = ({ cartTotal }) => (
  <Grid.Row only='computer tablet'>
    <Grid.Column width={4} floated='right' textAlign='center'>
      <Segment.Group>
        <Segment>
          <Price value={cartTotal}/>
        </Segment>
        <Segment>
          <CheckoutModal/>
        </Segment>
      </Segment.Group>
    </Grid.Column>
  </Grid.Row>
)

export default CartSummary
