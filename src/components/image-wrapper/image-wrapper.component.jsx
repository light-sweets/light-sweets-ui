import React from 'react'
import Img from 'gatsby-image'
import Logo from '../../images/default-image.inline.svg'

const ImageWrapper = ({ image, name }) => {
  return image
    ? <Img fluid={image.childImageSharp.fluid} alt={name}/>
    : <Logo/>
}

export default ImageWrapper
