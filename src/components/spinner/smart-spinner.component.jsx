import React from 'react'
import { useDispatch } from 'react-redux'
import Lottie from 'react-lottie'
import greenTickData from '../../animations/tick-green.json'
import { Dimmer, Loader } from 'semantic-ui-react'
import { clearLoginStatus } from '../../redux/user/user.slice'
import { closeModal } from '../../redux/modal/modals.slice'
import { UserStatus } from '../../redux/user/user.statuses'

const SmartSpinner = ({ status }) => {
  const dispatch = useDispatch()

  const defaultOptions = {
    loop: false,
    autoplay: true,
    animationData: greenTickData,
    rendererSettings: {
      preserveAspectRatio: 'xMidYMid slice'
    }
  }

  return (
    <Dimmer active={status === UserStatus.LOADING || status === UserStatus.SUCCESS} inverted>
      <Loader size='big' disabled={status === UserStatus.SUCCESS}/>
      {status === UserStatus.SUCCESS
        ? <Lottie options={defaultOptions}
                  height="4.5rem"
                  width="4.5rem"
                  eventListeners={[
                    {
                      eventName: 'complete',
                      callback: () => {
                        dispatch(clearLoginStatus())
                        dispatch(closeModal('loginOptionsModal'))
                        dispatch(closeModal('emailLoginModal'))
                      }
                    }
                  ]}
        />
        : null}
    </Dimmer>
  )
}

export default SmartSpinner
