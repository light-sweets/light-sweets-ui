import { Button } from 'semantic-ui-react'
import React from 'react'
import { useDispatch } from 'react-redux'

const LoginProviderButton = ({ provider, providerLabel, ProviderIcon }) => {
  const dispatch = useDispatch()
  const handleConnect = () => {
    const height = window.innerHeight / 1.5
    const width = window.innerWidth / 2
    const top = (window.innerHeight - height) / 2
    const left = (window.innerWidth - width) / 2
    const windowFeatures = `
        location=yes,
        resizable=yes,
        scrollbars=yes,
        status=yes,
        top=${top},
        left=${left},
        height=${height},
        width=${width}
    `
    window.dispatch = dispatch
    window.open(`${process.env.STRAPI_HOST}${process.env.STRAPI_PORT}/connect/${provider}`, providerLabel, windowFeatures)
  }
  return (
    <Button onClick={handleConnect} icon={ProviderIcon} label={providerLabel} circular/>
  )
}

export default LoginProviderButton
