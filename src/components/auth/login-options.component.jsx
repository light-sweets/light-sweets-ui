import { Button, List, Message, Segment } from 'semantic-ui-react'
import React from 'react'
import LoginProviderButton from './login-provider-button.component'
import { withTranslation } from 'react-i18next'
import GoogleLogo from '../../images/btn-google-light-normal.inline.svg'
import InstagramLogo from '../../images/btn-instagram.inline.svg'
import { useDispatch, useSelector } from 'react-redux'
import EmailLogo from '../../images/login-email.inline.svg'
import { closeModal, openModal } from '../../redux/modal/modals.slice'
import SmartSpinner from '../spinner/smart-spinner.component'
import { selectUserLoginError, selectUserLoginStatus } from '../../redux/user/user.selectors'
import { UserStatus } from '../../redux/user/user.statuses'
import { clearLoginStatus } from '../../redux/user/user.slice'

const LoginOptions = ({ t }) => {
  const dispatch = useDispatch()
  const loginError = useSelector(selectUserLoginError)
  const loginStatus = useSelector(selectUserLoginStatus)
  return (
    <Segment>
      <List relaxed>
        <List.Item>
          <LoginProviderButton provider='google'
                               ProviderIcon={<GoogleLogo/>}
                               providerLabel='Sing in with Google'/>
        </List.Item>
        <List.Item>
          <LoginProviderButton provider='instagram'
                               ProviderIcon={<InstagramLogo/>}
                               providerLabel='Sing in with Instagram'/>
        </List.Item>
        <List.Item>
          <Button icon={<EmailLogo/>}
                  label={t('Auth.modal.trigger')}
                  circular
                  onClick={() => {
                    dispatch(closeModal('loginOptionsModal'))
                    dispatch(openModal('emailLoginModal'))
                    dispatch(clearLoginStatus())
                  }}
          />
        </List.Item>
        <List.Item>
          <Message error
                   hidden={loginStatus !== UserStatus.ERROR}
                   content={t(loginError)}
                   onDismiss={() => dispatch(clearLoginStatus())}/>
        </List.Item>
      </List>
      <SmartSpinner status={loginStatus}/>
    </Segment>
  )
}

export default withTranslation()(LoginOptions)
