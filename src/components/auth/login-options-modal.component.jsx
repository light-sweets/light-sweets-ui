import { Modal } from 'semantic-ui-react'
import React from 'react'
import { withTranslation } from 'react-i18next'
import { useDispatch, useSelector } from 'react-redux'
import LoginOptions from './login-options.component'
import { closeModal } from '../../redux/modal/modals.slice'
import { selectLoginOptionsModalOpened } from '../../redux/modal/modal.selectors'

const LoginOptionsModal = ({ t }) => {
  const loginOptionModalOpened = useSelector(selectLoginOptionsModalOpened)
  const dispatch = useDispatch()
  return (
    <Modal closeIcon
           size='tiny'
           open={loginOptionModalOpened}
           onClose={() => dispatch(closeModal('loginOptionsModal'))}
    >
      <Modal.Header>{t('Auth.modal-options.header')}</Modal.Header>
      <Modal.Content>
        <LoginOptions/>
      </Modal.Content>
    </Modal>
  )
}

export default withTranslation()(LoginOptionsModal)
