import { Button, Modal, Segment } from 'semantic-ui-react'
import React from 'react'
import { withTranslation } from 'react-i18next'
import LoginForm from './form/login-form.component'
import { useSelector } from 'react-redux'
import LoginWithFormik from './form/login-formik-wrapper.component'
import { closeAllModals, closeModal, openModal } from '../../redux/modal/modals.slice'
import { selectEmailLoginModalOpened } from '../../redux/modal/modal.selectors'
import { clearLoginStatus } from '../../redux/user/user.slice'

const LoginModal = ({ t, dispatch }) => {
  const emailLoginModalOpened = useSelector(selectEmailLoginModalOpened)

  const handleClose = () => {
    dispatch(closeAllModals())
    dispatch(clearLoginStatus())
  }

  return (
    <Modal closeIcon
           size='tiny'
           open={emailLoginModalOpened}
           onClose={() => handleClose()}
    >
      <Modal.Header>{t('Auth.modal.header')}</Modal.Header>
      <Modal.Content><LoginForm/></Modal.Content>
      <Modal.Actions as={Segment}>
        <Button content={'Register'}
                label={t('Register.button.label')}
                labelPosition='left'
                onClick={() => {
                  dispatch(closeModal('emailLoginModal'))
                  dispatch(openModal('registerModal'))
                }}/>
      </Modal.Actions>
    </Modal>
  )
}

export default withTranslation()(LoginWithFormik(LoginModal))
