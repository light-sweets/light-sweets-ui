import { withFormik } from 'formik'
import { object, string } from 'yup'
import { register } from '../../../redux/user/user.slice'

const initialValues = {
  phoneNumber: '',
  email: '',
  password: ''
}

const RegisterWithFormik = withFormik({
  mapPropsToValues: () => initialValues,
  handleSubmit: (values, { props, setSubmitting }) => {
    const { dispatch } = props
    dispatch(register({
      url: `${process.env.STRAPI_HOST}${process.env.STRAPI_PORT}${process.env.STRAPI_REGISTER_ENDPOINT}`,
      json: {
        username: values.phoneNumber,
        email: values.email,
        password: values.password
      }
    })).then(() => setSubmitting(false))
  },
  validationSchema: ({ t }) => object().shape({
    phoneNumber: string().required(t('Auth.form.error.phone-number.provide'))
                         .matches(/^\+?3?8?(0\d{9})$/, t(('Auth.form.error.phone-number.format'))),
    email: string().required(t('Auth.form.error.email.provide')).email(t('Auth.form.error.email.format')),
    password: string().required(t('Auth.form.error.password.provide'))
  })
})

export default RegisterWithFormik
