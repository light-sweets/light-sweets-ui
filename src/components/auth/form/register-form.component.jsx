import { Form, Message, Segment } from 'semantic-ui-react'
import React from 'react'
import { withTranslation } from 'react-i18next'
import { Field, Form as FormikForm, useFormikContext } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { selectUserLoginError, selectUserLoginStatus } from '../../../redux/user/user.selectors'
import SmartSpinner from '../../spinner/smart-spinner.component'
import { UserStatus } from '../../../redux/user/user.statuses'
import { clearLoginStatus } from '../../../redux/user/user.slice'

const RegisterForm = ({ t }) => {
  const { errors } = useFormikContext()
  const dispatch = useDispatch()
  const loginStatus = useSelector(selectUserLoginStatus)
  const loginError = useSelector(selectUserLoginError)

  return (
    <Segment>
      <Form as={FormikForm}
            success={loginStatus === UserStatus.SUCCESS}
            error={loginStatus === UserStatus.ERROR}
            loading={loginStatus === UserStatus.LOADING}
      >
        <Field as={Form.Input}
               name='phoneNumber'
               label={t('Auth.form.phoneNumber')}
               required
               error={errors.phoneNumber}
        />
        <Field as={Form.Input}
               name='email'
               label={t('Auth.form.email')}
               required
               error={errors.email}
               type='email'
        />
        <Field as={Form.Input}
               name='password'
               label={t('Auth.form.password')}
               required
               error={errors.password}
               type='password'
        />
        <Message
          error
          content={t(loginError)}
          header={t('Auth.form.error.server')}
          hidden={loginStatus !== UserStatus.ERROR}
          onDismiss={() => dispatch(clearLoginStatus())}
        />
      </Form>
      <SmartSpinner status={loginStatus}/>
    </Segment>
  )
}

export default withTranslation()(RegisterForm)
