import { withFormik } from 'formik'
import { object, string } from 'yup'
import { login } from '../../../redux/user/user.slice'

const initialValues = {
  email: '',
  password: ''
}

const LoginWithFormik = withFormik({
  mapPropsToValues: () => initialValues,
  handleSubmit: (values, { props, setSubmitting }) => {
    const { dispatch } = props
    dispatch(login({
      url: `${process.env.STRAPI_HOST}${process.env.STRAPI_PORT}${process.env.STRAPI_AUTH_ENDPOINT}`,
      json: {
        identifier: values.email,
        password: values.password
      }
    })).then(() => setSubmitting(false))
  },
  validationSchema: ({ t }) => object().shape({
    email: string().required(t('Auth.form.error.email.provide')).email(t('Auth.form.error.email.format')),
    password: string().required(t('Auth.form.error.password.provide'))
  })
})

export default LoginWithFormik
