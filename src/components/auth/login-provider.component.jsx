import React, { useEffect } from 'react'
import { login } from '../../redux/user/user.slice'
import { withTranslation } from 'react-i18next'
import { Container, Loader } from 'semantic-ui-react'

const LoginProvider = ({ location, providerName, t }) => {
  useEffect(() => {
    window.opener.dispatch(login(
      {
        url: `${process.env.STRAPI_HOST}${process.env.STRAPI_PORT}/auth/${providerName}/callback${location.search}`
      }
    )).then(() => window.close())
  }, [location.search, providerName])

  return (
    <Container>
      <Loader active content={t('Auth.provider.message.pending')} size='massive'/>
    </Container>
  )
}

export default withTranslation()(LoginProvider)
