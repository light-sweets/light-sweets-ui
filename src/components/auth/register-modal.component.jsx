import { Button, Modal } from 'semantic-ui-react'
import React from 'react'
import { withTranslation } from 'react-i18next'
import { useSelector } from 'react-redux'
import { closeAllModals, closeModal, openModal } from '../../redux/modal/modals.slice'
import { selectRegisterModalOpened } from '../../redux/modal/modal.selectors'
import { clearLoginStatus } from '../../redux/user/user.slice'
import RegisterForm from './form/register-form.component'
import RegisterWithFormik from './form/register-formik-wrapper.component'

const RegisterModal = ({ t, submitForm, dispatch }) => {
  const registerModalOpened = useSelector(selectRegisterModalOpened)

  const handleClose = () => {
    dispatch(closeAllModals())
    dispatch(clearLoginStatus())
  }

  const handleBack = () => {
    dispatch(closeModal('registerModal'))
    dispatch(openModal('emailLoginModal'))
    dispatch(clearLoginStatus())
  }

  return (
    <>
      <Modal closeIcon
             size='tiny'
             open={registerModalOpened}
             onClose={() => handleClose()}
      >
        <Modal.Header>{t('Register.modal.header')}</Modal.Header>
        <Modal.Content><RegisterForm/></Modal.Content>
        <Modal.Actions>
          <Button content='Back' onClick={() => handleBack()}/>
          <Button content='Register' onClick={() => submitForm()} primary/>
        </Modal.Actions>
      </Modal>
    </>
  )
}

export default withTranslation()(RegisterWithFormik(RegisterModal))
