import React from 'react'
import { Link } from 'gatsby'
import { Container, Input, Menu } from 'semantic-ui-react'
import ShoppingCartIcon from './cart-icon.component'
import LangSwitch from './lang-switch.component'
import UserSubmenu from './user-submenu.component'

const DesktopMenu = () => {
  return (
    <Menu size="huge" borderless pointing>
      <Container text>
        <Menu.Item as={Link} to="/" header>
          Starter Store
        </Menu.Item>
        <Menu.Item>
          <Input icon='search' placeholder='Search...'/>
        </Menu.Item>
        <Menu.Item as={Link} to="/cart/">
          <ShoppingCartIcon name="Cart"/>
        </Menu.Item>
        <Menu.Item>
          <LangSwitch/>
        </Menu.Item>
        <UserSubmenu/>
      </Container>
    </Menu>
  )
}

export default DesktopMenu
