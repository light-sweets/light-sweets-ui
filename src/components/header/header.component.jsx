import React from 'react'
import DesktopMenu from './desktop-menu.component'
import { MediaContextProvider, Media } from '../../media/media'

import MobileMenu from './mobile-menu.component'

const Header = () => {
  return (
    <MediaContextProvider>
      <Media at='mobile'>
        <MobileMenu/>
      </Media>
      <Media greaterThan='mobile'>
        <DesktopMenu/>
      </Media>
    </MediaContextProvider>
  )
}

export default Header
