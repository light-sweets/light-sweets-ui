import React from 'react'
import { Dropdown } from 'semantic-ui-react'
import { switchLang } from '../../redux/lang/lang.slice'
import { withTranslation } from 'react-i18next'
import { selectCurrentLang } from '../../redux/lang/lang.selectors'
import { useDispatch, useSelector } from 'react-redux'

const LangSwitch = ({ i18n }) => {
  const currentLang = useSelector(selectCurrentLang)
  const dispatch = useDispatch()

  const options = [
    {
      key: 'en',
      text: 'en',
      value: 'en',
      flag: 'gb'
    },
    {
      key: 'ru',
      text: 'ru',
      value: 'ru',
      flag: 'ru'
    },
    {
      key: 'ua',
      text: 'ua',
      value: 'ua',
      flag: 'ua'
    }
  ]

  const handleChange = (e) => {
    const newLang = e.target.innerText
    dispatch(switchLang(newLang))
    i18n.changeLanguage(newLang)
  }

  return (
    <Dropdown button
              className='icon'
              floating
              labeled
              icon='world' options={options}
              defaultValue={currentLang || i18n.languages[1]}
              onChange={e => handleChange(e)}
    />
  )
}

export default withTranslation()(LangSwitch)
