import React from 'react'
import { Button, Dropdown, Menu } from 'semantic-ui-react'
import { useDispatch, useSelector } from 'react-redux'
import { logout } from '../../redux/user/user.slice'
import { selectCurrentUser } from '../../redux/user/user.selectors'
import { Link } from 'gatsby'
import { closeSidebar } from '../../redux/sidebar/sidebar.slice'
import { openModal } from '../../redux/modal/modals.slice'
import { withTranslation } from 'react-i18next'

const UserSubmenu = ({ t }) => {
  const currentUser = useSelector(selectCurrentUser)
  const dispatch = useDispatch()

  const AuthorizedUserSubmenu = () => (
    <Dropdown item text={currentUser.username}>
      <Dropdown.Menu>
        <Dropdown.Item
          as={Link} to='/user'
          onClick={() => dispatch(closeSidebar())}
          content={'Profile'}/>
        <Dropdown.Item content={
          <Button content='Logout'
                  onClick={() => {
                    dispatch(logout())
                    dispatch(closeSidebar())
                  }}
          />}/>
      </Dropdown.Menu>
    </Dropdown>
  )

  const LoginOptionsModalItem = () => (
    <Menu.Item>
      <Button content={t('login')}
              onClick={() => dispatch(openModal('loginOptionsModal'))}
              primary
      />
    </Menu.Item>
  )

  return (
    currentUser ? <AuthorizedUserSubmenu/> : <LoginOptionsModalItem/>
  )
}

export default withTranslation()(UserSubmenu)
