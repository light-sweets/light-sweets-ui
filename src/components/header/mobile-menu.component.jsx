import React from 'react'
import { Link } from 'gatsby'
import { Button, Container, Menu } from 'semantic-ui-react'
import ShoppingCartIcon from './cart-icon.component'
import { useDispatch } from 'react-redux'
import { openSidebar } from '../../redux/sidebar/sidebar.slice'

const MobileMenu = () => {
  const dispatch = useDispatch()
  return (
    <Menu size="huge" borderless pointing>
      <Container text>
        <Menu.Item position="left">
          <Button
            icon='sidebar'
            basic
            onClick={() => dispatch(openSidebar())}
            aria-label="Open Navigation Menu"
          />
        </Menu.Item>
        <Menu.Item
          as={Link}
          to="/"
          header
        >
          Store
        </Menu.Item>
        <Menu.Item position='right'
                   as={Link}
                   to="/cart/"
        >
          <ShoppingCartIcon/>
        </Menu.Item>
      </Container>
    </Menu>
  )
}

export default MobileMenu
