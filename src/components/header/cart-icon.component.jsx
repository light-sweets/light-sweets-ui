import React from 'react'
import { Icon } from 'semantic-ui-react'
import { useSelector } from 'react-redux'
import { selectCartItemsCount } from '../../redux/cart/cart.selectors'

const ShoppingCartIcon = ({ name = '' }) => {
  const cartCount = useSelector(selectCartItemsCount)

  const showCartCount = () => {
    if (!cartCount) {
      return '(0)'
    }
    if (cartCount > 9) {
      return (
        <span style={{ fontSize: 'smaller' }}>
          9<sup>+</sup>
        </span>
      )
    }
    return `(${cartCount})`
  }

  return (
    <div>
      <Icon name="cart"/>
      {` ${name} `}
      {showCartCount()}
    </div>
  )
}

export default ShoppingCartIcon
