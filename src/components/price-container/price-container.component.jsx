import { withTranslation } from 'react-i18next'

const Price = ({ value, t }) => {
  return `${value / 100} ${t('currency')}`
}

export default withTranslation()(Price)
