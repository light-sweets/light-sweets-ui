import React from 'react'
import { selectBillingActive } from '../../../redux/checkout/checkout.selectors'
import { useDispatch, useSelector } from 'react-redux'
import { Button } from 'semantic-ui-react'
import { setActiveStep } from '../../../redux/checkout/checkout.slice'

const SecondaryAction = ({ setOpen }) => {
  const billingOpen = useSelector(selectBillingActive)
  const dispatch = useDispatch()

  const buttonProps = billingOpen
    ? { content: 'Back to shipping', secondary: true, onClick: () => dispatch(setActiveStep('shipping')) }
    : { content: 'Back to cart', secondary: true, onClick: () => setOpen(false) }
  return <Button {...buttonProps}/>
}

export default SecondaryAction
