import React from 'react'
import { selectBillingActive } from '../../../redux/checkout/checkout.selectors'
import { setActiveStep } from '../../../redux/checkout/checkout.slice'
import { useDispatch, useSelector } from 'react-redux'
import { useFormikContext } from 'formik'
import { Button } from 'semantic-ui-react'

const PrimaryAction = () => {
  const { validateForm } = useFormikContext()
  const billingOpen = useSelector(selectBillingActive)
  const dispatch = useDispatch()

  const buttonProps = billingOpen
    ? { content: 'Pay', primary: true }
    : {
      content: 'Billing',
      primary: true,
      onClick: () => {
        validateForm().then(() => {
          dispatch(setActiveStep('billing'))
        })
      }
    }
  return <Button {...buttonProps}/>
}

export default PrimaryAction
