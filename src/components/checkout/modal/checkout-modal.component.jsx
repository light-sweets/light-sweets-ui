import React, { useState } from 'react'
import { Modal } from 'semantic-ui-react'
import CheckoutButton from './checkout-button.component'
import CheckoutForm from '../form/checkout-form.component'
import { useDispatch } from 'react-redux'
import { setActiveStep } from '../../../redux/checkout/checkout.slice'
import CheckoutStepsBar from '../steps/checkout-steps-bar.component'
import CheckoutWithFormik from '../form/checkout-formik-wrapper.component'
import SecondaryAction from './secondary-action.component'
import PrimaryAction from './primary-action.component'

const CheckoutModal = () => {
  const [open, setOpen] = useState(false)
  const dispatch = useDispatch()

  return (
    <Modal trigger={<CheckoutButton/>}
           open={open}
           onOpen={() => {
             dispatch(setActiveStep('shipping'))
             setOpen(true)
           }}
           onClose={() => setOpen(false)}
           closeOnDimmerClick={false}
           closeIcon
    >
      <Modal.Header content={<CheckoutStepsBar/>}/>
      <Modal.Content content={<CheckoutForm/>}/>
      <Modal.Actions>
        <SecondaryAction setOpen={setOpen}/>
        <PrimaryAction/>
      </Modal.Actions>
    </Modal>
  )
}

export default CheckoutWithFormik(CheckoutModal)
