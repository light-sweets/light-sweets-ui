import { Button, Icon } from 'semantic-ui-react'
import React from 'react'
import { withTranslation } from 'react-i18next'

const CheckoutButton = ({ t, ...otherProps }) => (
  <Button animated primary {...otherProps}>
    <Button.Content visible>{t('checkout')}</Button.Content>
    <Button.Content hidden>
      <Icon name='arrow right'/>
    </Button.Content>
  </Button>
)

export default withTranslation()(CheckoutButton)
