import { withFormik } from 'formik'
import { string, object } from 'yup'

const initialValues = {
  name: '',
  surname: '',
  patronymic: '',
  address: '',
  phoneNumber: '',
  email: ''
}

const CheckoutWithFormik = withFormik({
  mapPropsToValues: () => initialValues,
  handleSubmit: () => {
    console.log('submitting')
  },
  validationSchema: () => object().shape({
    name: string().required('Required')
  })
})

export default CheckoutWithFormik
