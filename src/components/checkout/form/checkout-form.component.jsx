import React from 'react'
import { useSelector } from 'react-redux'
import { Form, Segment } from 'semantic-ui-react'
import { Form as FormikForm } from 'formik'
import { withTranslation } from 'react-i18next'
import { selectBillingActive } from '../../../redux/checkout/checkout.selectors'
import BillingStep from '../steps/billing-step.component'
import ShippingStep from '../steps/shipping-step.component'

const CheckoutForm = () => {
  const billingOpen = useSelector(selectBillingActive)
  return (
    <Segment>
      <Form as={FormikForm}>
        {billingOpen ? <BillingStep/> : <ShippingStep/>}
      </Form>
    </Segment>
  )
}
export default withTranslation()(CheckoutForm)
