import React from 'react'
import { withTranslation } from 'react-i18next'
import { Form } from 'semantic-ui-react'
import { Field, useFormikContext } from 'formik'
import LoginProviderButton from '../../auth/login-provider-button.component'

const ShippingStep = ({ t }) => {
  const { errors } = useFormikContext()

  return (
    <>
      <LoginProviderButton provider='google' providerLabel='Google' providerIcon='google'/>
      <Form.Group>
        <Field as={Form.Input}
               name='name'
               label={t('name')}
               required
               error={errors.name}/>
        <Field as={Form.Input}
               name='surname'
               label={t('surname')}
               required
               error={errors.surname}/>
        <Field as={Form.Input}
               name='patronymic'
               label={t('patronymic')}
               required
               error={errors.patronymic}/>
      </Form.Group>
      <Form.Group>
        <Field as={Form.Input}
               name='address'
               label={t('address')}
               required
               width={8}
               error={errors.address}/>
        <Field as={Form.Input}
               name='phoneNumber'
               label={t('phoneNumber')}
               required
               width={5}
               error={errors.phoneNumber}/>
        <Field as={Form.Input}
               name='email'
               type="email"
               label={t('email')}
               required
               error={errors.email}/>
      </Form.Group>
    </>
  )
}

export default withTranslation()(ShippingStep)
