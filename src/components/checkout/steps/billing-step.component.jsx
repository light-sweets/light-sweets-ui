import React from 'react'
import { withTranslation } from 'react-i18next'

const BillingStep = ({ t }) => (
  <div>LOLOOP:LL:OP</div>
)

export default withTranslation()(BillingStep)
