import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Segment, Step } from 'semantic-ui-react'
import { selectStepsStatus } from '../../../redux/checkout/checkout.selectors'
import { setActiveStep } from '../../../redux/checkout/checkout.slice'

const CheckoutStepsBar = () => {
  const stepsStatus = useSelector(selectStepsStatus)
  const dispatch = useDispatch()

  const steps = [
    {
      key: 'shipping',
      active: stepsStatus.shipping,
      icon: 'truck',
      title: 'Shipping',
      description: 'Choose your shipping options',
      link: true,
      onClick: () => dispatch(setActiveStep('shipping'))
    },
    {
      key: 'billing',
      active: stepsStatus.billing,
      icon: 'payment',
      title: 'Billing',
      description: 'Enter billing information',
      link: true,
      onClick: () => {
        dispatch(setActiveStep('billing'))
      }
    }
  ]
  return (
    <Segment>
      <Step.Group items={steps}/>
    </Segment>
  )
}

export default CheckoutStepsBar
