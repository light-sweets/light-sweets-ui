import React from 'react'
import { Menu, Sidebar } from 'semantic-ui-react'
import { Link } from 'gatsby'
import UserSubmenu from '../header/user-submenu.component'
import { useDispatch, useSelector } from 'react-redux'
import { selectSidebarVisible } from '../../redux/sidebar/sidebar.selectors'
import { closeSidebar } from '../../redux/sidebar/sidebar.slice'

const SidebarMenu = () => {
  const visible = useSelector(selectSidebarVisible)
  const dispatch = useDispatch()
  return (
    <Sidebar
      as={Menu}
      animation='overlay'
      icon='labeled'
      inverted
      onHide={() => dispatch(closeSidebar())}
      vertical
      visible={visible}
      width='thin'
    >
      <Menu.Item>
        <Link to="/" onClick={() => dispatch(closeSidebar())}>
          Home
        </Link>
      </Menu.Item>
      <Menu.Item>
        <Link to="/cart/" onClick={() => dispatch(closeSidebar())}>
          {'Shopping Cart'}
        </Link>
      </Menu.Item>
      <UserSubmenu/>
   </Sidebar>
  )
}

export default SidebarMenu
