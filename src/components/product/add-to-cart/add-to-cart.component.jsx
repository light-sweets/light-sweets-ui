import React, { useState } from 'react'
import { Input } from 'semantic-ui-react'
import { addItemToCart } from '../../../redux/cart/cart.slice'
import { useDispatch } from 'react-redux'
import { withTranslation } from 'react-i18next'

const AddToCart = ({ product, t }) => {
  const [quantity, setQuantity] = useState('1')
  const [error, setError] = useState('')
  const dispatch = useDispatch()

  const validateQuantity = quantity => {
    const re = /^[0-9\b]+$/
    return !re.test(quantity) ? t('errors.quantityValidation') : ''
  }
  const handleAddItem = () => {
    const error = validateQuantity(quantity)
    setError(error)
    if (!error) {
      dispatch(addItemToCart({ item: product, quantity: parseInt(quantity) }))
    }
  }

  const handleChangeQuantity = ({ target: { value } }) => {
    const error = validateQuantity(value)
    setError(error)
    setQuantity(value)
  }

  return (
    <>
      <Input
        type="number"
        placeholder="Quantity"
        value={quantity}
        min={1}
        step={1}
        error={!!error}
        onChange={handleChangeQuantity}
        action={{
          color: 'orange',
          content: t('addToCart'),
          icon: 'plus cart',
          onClick: handleAddItem
        }}
      />
      {error &&
        <div style={{ color: 'red', position: 'absolute' }}>{error}</div>
      }
    </>
  )
}

export default withTranslation()(AddToCart)
