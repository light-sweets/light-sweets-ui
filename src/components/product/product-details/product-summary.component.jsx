import React from 'react'

import { Item, Label } from 'semantic-ui-react'
import AddToCart from '../add-to-cart/add-to-cart.component'
import { withTranslation } from 'react-i18next'
import Price from '../../price-container/price-container.component'
import ImageWrapper from '../../image-wrapper/image-wrapper.component'

const ProductSummary = ({ product, t }) => {
  return (
    <Item.Group>
      <Item style={{ alignItems: 'center' }}>
        <Item.Image size='medium'>
          <ImageWrapper image={product.image} name={product.name}/>
        </Item.Image>
        <Item.Content>
          <Item.Header>{product.name}</Item.Header>
          <Item.Description>
            <Label>{`${t('sku')}: ${product.sku}`}</Label>
          </Item.Description>
          <Item.Description>
            <Label>{t('price')} <Price value={product.price}/></Label>
          </Item.Description>
          <Item.Description>
            {
              product.inStock
                ? <Label color='green'>{t('inStock')}</Label>
                : <Label color='red'>{t('outOfStock')}</Label>
            }
          </Item.Description>
          <Item.Description>
            {
              product.inStock
                ? <AddToCart product={product}/>
                : null
            }
          </Item.Description>
        </Item.Content>
      </Item>
    </Item.Group>
  )
}

export default withTranslation()(ProductSummary)
