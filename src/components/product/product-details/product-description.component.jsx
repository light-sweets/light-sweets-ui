import React from 'react'
import { Divider, Header } from 'semantic-ui-react'
import { withTranslation } from 'react-i18next'
import ReactMarkdown from 'react-markdown/with-html'

const ProductDescription = ({ description, t }) => (
  <div>
    <Header as="h3">{t('pdp.about')}</Header>
    <div><ReactMarkdown source={description} /></div>
    <Divider/>
  </div>
)

export default withTranslation()(ProductDescription)
