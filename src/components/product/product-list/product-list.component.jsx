import React from 'react'
import { Card } from 'semantic-ui-react'
import { Link } from 'gatsby'
import Price from '../../price-container/price-container.component'
import ImageWrapper from '../../image-wrapper/image-wrapper.component'

const ProductList = ({ products }) => {
  const mapProductsToItems = products => {
    return products.map(({ node: { name, id, image, price, slug } }) => {
      return {
        as: Link,
        to: `/product/${slug}/`,
        childKey: id,
        image: <ImageWrapper image={image} name={name}/>,
        header: name,
        meta: <Card.Meta style={{ color: 'dimgray' }}><Price value={price}/></Card.Meta>
      }
    })
  }

  return (
    <Card.Group
      items={mapProductsToItems(products)}
      itemsPerRow={2}
      stackable
    />
  )
}

export default ProductList
