import React from 'react'
import Headroom from 'react-headroom'
import { Container, Sidebar } from 'semantic-ui-react'
import Footer from '../footer/footer.component'
import Header from '../header/header.component'
import SidebarMenu from '../sidebar/sidebar-menu.component'
import LoginOptionsModal from '../auth/login-options-modal.component'
import LoginModal from '../auth/login-modal.component'
import RegisterModal from '../auth/register-modal.component'
import { useDispatch } from 'react-redux'

const Layout = ({ location, children }) => {
  const dispatch = useDispatch()
  return (
    <>
      <LoginOptionsModal/>
      <LoginModal dispatch={dispatch}/>
      <RegisterModal dispatch={dispatch}/>
      <Headroom
        upTolerance={10}
        downTolerance={10}
        style={{ zIndex: '20' }}
      >
        <Header location={location}/>
      </Headroom>
      <Sidebar.Pushable>
        <SidebarMenu/>
        <Sidebar.Pusher>
          <Container text>{children}</Container>
          <Footer/>
        </Sidebar.Pusher>
      </Sidebar.Pushable>
    </>
  )
}

export default Layout
