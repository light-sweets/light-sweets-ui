import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = {
  value: ''
}

const langSlice = createSlice({
  name: 'lang',
  initialState: INITIAL_STATE,
  reducers: {
    switchLang: (state, action) => {
      state.value = action.payload
    }
  }
})

export const { switchLang } = langSlice.actions
export default langSlice
