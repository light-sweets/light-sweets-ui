import { createSelector } from 'reselect'

const selectLang = state => state.langSwitch

export const selectCurrentLang = createSelector(
  [selectLang],
  lang => lang.value
)
