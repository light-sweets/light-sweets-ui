import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = {
  stepsStatus: { shipping: true, billing: false }
}

const checkoutSlice = createSlice({
  name: 'checkout',
  initialState: INITIAL_STATE,
  reducers: {
    setActiveStep: (state, action) => {
      Object.keys(state.stepsStatus).forEach(key => {
        state.stepsStatus[key] = key === action.payload
      })
    }
  }
})

export const { setActiveStep } = checkoutSlice.actions
export default checkoutSlice
