import { createSelector } from 'reselect'

const selectCheckout = state => state.checkout

export const selectStepsStatus = createSelector(
  [selectCheckout],
  checkout => checkout.stepsStatus
)

export const selectBillingActive = createSelector(
  [selectStepsStatus],
  stepsStatus => stepsStatus.billing
)
