import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = {
  cartItems: []
}

const findExistingItem = (items, itemId) => {
  return items.find(item => item.id === itemId)
}

const cartSlice = createSlice({
  name: 'cart',
  initialState: INITIAL_STATE,
  reducers: {
    addItemToCart: (state, action) => {
      const existingCartItem = findExistingItem(state.cartItems, action.payload.item.id)

      if (existingCartItem) {
        existingCartItem.quantity += action.payload.quantity
      } else {
        state.cartItems.push({ ...action.payload.item, quantity: action.payload.quantity })
      }
    },
    updateItemQuantity: (state, action) => {
      const existingCartItem = findExistingItem(state.cartItems, action.payload.item.id)

      if (existingCartItem) {
        existingCartItem.quantity = action.payload.quantity
      }
    },
    clearItemFromCart: (state, action) => {
      state.cartItems = state.cartItems.filter(item => item.id !== action.payload.id)
    }
  }
})

export const { addItemToCart, updateItemQuantity, clearItemFromCart } = cartSlice.actions

export default cartSlice
