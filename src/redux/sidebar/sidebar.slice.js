import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = {
  visible: false
}

const sidebarSlice = createSlice(
  {
    name: 'sidebar',
    initialState: INITIAL_STATE,
    reducers: {
      openSidebar: (state) => {
        state.visible = true
      },
      closeSidebar: (state) => {
        state.visible = false
      }
    }
  }
)

export const { openSidebar, closeSidebar } = sidebarSlice.actions

export default sidebarSlice
