import { createSelector } from 'reselect'

const selectSidebar = state => state.sidebar

export const selectSidebarVisible = createSelector(
  [selectSidebar],
  sidebar => sidebar.visible
)
