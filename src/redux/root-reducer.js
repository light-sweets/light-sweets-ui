import { combineReducers } from 'redux'

import cartSlice from './cart/cart.slice'
import checkoutSlice from './checkout/checkout.slice'
import langSlice from './lang/lang.slice'
import userSlice from './user/user.slice'
import modalsSlice from './modal/modals.slice'
import sidebarSlice from './sidebar/sidebar.slice'

export default combineReducers({
  cart: cartSlice.reducer,
  langSwitch: langSlice.reducer,
  checkout: checkoutSlice.reducer,
  user: userSlice.reducer,
  modals: modalsSlice.reducer,
  sidebar: sidebarSlice.reducer
})
