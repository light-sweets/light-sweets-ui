import React from 'react'
import { Provider } from 'react-redux'
import logger from 'redux-logger'
import { persistReducer, persistStore } from 'redux-persist'
import storage from 'redux-persist/lib/storage'
import rootReducer from './root-reducer'
import { PersistGate } from 'redux-persist/integration/react'
import { I18nextProvider } from 'react-i18next'
import i18n from '../i18n'
import { MediaContextProvider } from '../media/media'
import 'semantic-ui-less/semantic.less'
import { configureStore } from '@reduxjs/toolkit'

const persistConfig = {
  key: 'root',
  storage
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

const store = configureStore({
    reducer: persistedReducer,
    middleware: getDefaultMiddleware => getDefaultMiddleware().concat(logger),
    devTools: process.env.NODE_ENV !== 'production'
  }
)

const ReduxWrapper = ({ element }) => (
  <Provider store={store}>
    <I18nextProvider i18n={i18n}>
      <MediaContextProvider>
        <PersistGate
          loading={null}
          persistor={persistStore(store)}>
          {element}
        </PersistGate>
      </MediaContextProvider>
    </I18nextProvider>
  </Provider>
)

export default ReduxWrapper
