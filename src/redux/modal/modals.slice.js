import { createSlice } from '@reduxjs/toolkit'

const INITIAL_STATE = {
  emailLoginModal: false,
  loginOptionsModal: false,
  registerModal: false
}

const modalsSlice = createSlice(
  {
    name: 'modals',
    initialState: INITIAL_STATE,
    reducers: {
      openModal: (state, action) => {
        state[action.payload] = true
      },
      closeModal: (state, action) => {
        state[action.payload] = false
      },
      closeAllModals: (state) => {
        state.emailLoginModal = false
        state.loginOptionsModal = false
        state.registerModal = false
      }
    }
  }
)

export const { openModal, closeModal, closeAllModals } = modalsSlice.actions

export default modalsSlice
