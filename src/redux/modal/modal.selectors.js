import { createSelector } from 'reselect'

const selectModals = state => state.modals

export const selectEmailLoginModalOpened = createSelector(
  [selectModals],
  modals => modals.emailLoginModal
)

export const selectLoginOptionsModalOpened = createSelector(
  [selectModals],
  modals => modals.loginOptionsModal
)

export const selectRegisterModalOpened = createSelector(
  [selectModals],
  modals => modals.registerModal
)
