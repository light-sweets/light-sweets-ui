import { createSelector } from 'reselect'

const selectUser = state => state.user

export const selectCurrentUser = createSelector(
  [selectUser],
  user => user.currentUser
)

export const selectUserLoginStatus = createSelector(
  [selectUser],
  user => user.status
)

export const selectUserLoginError = createSelector(
  [selectUser],
  user => user.error
)
