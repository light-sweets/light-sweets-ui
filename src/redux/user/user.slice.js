import { createAsyncThunk, createSlice } from '@reduxjs/toolkit'
import ky from 'ky'
import { UserStatus } from './user.statuses'

const INITIAL_STATE = {
  currentUser: null,
  jwt: null,
  status: UserStatus.IDLE,
  error: null
}

const processResponse = (response, thunkApi) => {
  const errorMessage = response.message?.[0].messages?.[0].id
  return response.user ? response : thunkApi.rejectWithValue(errorMessage)
}

export const login = createAsyncThunk('user/login', async (payload, thunkApi) => {
  if (payload.json) {
    const response = await ky.post(payload.url, { throwHttpErrors: false, json: payload.json }).json()
    return processResponse(response, thunkApi)
  }
  const response = await ky.get(payload.url, { throwHttpErrors: false }).json()
  return processResponse(response, thunkApi)
})

export const register = createAsyncThunk('user/register', async (payload, thunkApi) => {
  const response = await ky.post(payload.url, { throwHttpErrors: false, json: payload.json }).json()
  return processResponse(response, thunkApi)
})

const userSlice = createSlice(
  {
    name: 'user',
    initialState: INITIAL_STATE,
    reducers: {
      logout: (state) => {
        state.currentUser = null
        state.jwt = null
        state.status = UserStatus.IDLE
        state.error = null
      },
      clearLoginStatus: state => {
        state.status = UserStatus.IDLE
        state.error = null
      }
    },
    extraReducers: {
      [login.pending]: (state) => {
        state.status = UserStatus.LOADING
      },
      [login.fulfilled]: (state, { payload }) => {
        state.status = UserStatus.SUCCESS
        state.currentUser = payload.user
        state.jwt = payload.jwt
      },
      [login.rejected]: (state, { payload, error }) => {
        state.status = UserStatus.ERROR
        state.error = payload || error.message
      },
      [register.pending]: (state) => {
        state.status = UserStatus.LOADING
      },
      [register.fulfilled]: (state, { payload }) => {
        state.status = UserStatus.SUCCESS
        console.log(payload)
      },
      [register.rejected]: (state, { payload, error }) => {
        state.status = UserStatus.ERROR
        state.error = payload || error.message
      }
    }
  }
)

export const { logout, clearLoginStatus } = userSlice.actions

export default userSlice
