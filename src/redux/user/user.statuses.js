export const UserStatus = {
  SUCCESS: 'success',
  ERROR: 'error',
  IDLE: 'idle',
  LOADING: 'loading'
}
