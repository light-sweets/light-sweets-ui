module.exports = {
  globals: {
    __PATH_PREFIX__: true
  },
  parser: 'babel-eslint',
  env: {
    browser: true,
    es2020: true
  },
  extends: [
    'standard',
    'react-app',
    'eslint:recommended',
    'plugin:react/recommended'
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 11,
    sourceType: 'module'
  },
  plugins: [
    'react',
    'graphql'
  ],
  rules: {
    'react/prop-types': 0,
    'graphql/template-strings': [
      'error',
      {
        env: 'relay'
      }
    ],
    indent: 'off'
  }
}
