FROM node:12 as build

WORKDIR "/gatsby"

COPY ./package.json ./
COPY ./package-lock.json ./

RUN npm install

COPY . .

RUN npm run build

EXPOSE 8003

CMD ["npm", "run", "serve"]
